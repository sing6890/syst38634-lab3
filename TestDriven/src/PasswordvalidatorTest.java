import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;



public class PasswordvalidatorTest {
	
	@Test
	public void testCheckPasswordLength() {
		boolean result = Passwordvalidator.checkPasswordLength("sing6890");
		Assert.assertTrue(result);
		
	}
	
	@Test
	public void testCheckDigits() {
		boolean result = Passwordvalidator.checkDigits("sing6890");
		Assert.assertTrue(result);
	}

	
	@Test(expected = RuntimeException.class)
	public void testCheckPasswordLengthException() {
		boolean result = Passwordvalidator.checkPasswordLength("tps26");
		Assert.assertTrue(result);
	}
	@Test
	public void testCheckPasswordLengthBoundaryIn() {
		boolean result = Passwordvalidator.checkPasswordLength("sheridan");
		Assert.assertTrue(result);
	}
	@Test
	public void testCheckPasswordLengthBoundaryOut() {
		boolean result = Passwordvalidator.checkPasswordLength("sh");
		Assert.assertTrue(result);
	}
	@Test
	public void testCheckPasswordLengthHappyPath() {
		boolean result = Passwordvalidator.checkPasswordLength("gps25ldh43lgh");
		Assert.assertTrue(result);
	}
	
	
	@Test(expected = RuntimeException.class)
	public void testCheckDigitsException() {
		boolean result = Passwordvalidator.checkDigits("tpssc");
		Assert.assertTrue(result);
	}
	@Test
	public void testCheckDigitsBoundaryIn() {
		boolean result = Passwordvalidator.checkDigits("sheridan25");
		Assert.assertTrue(result);
	}
	@Test
	public void testCheckDigitsBoundaryOut() {
		boolean result = Passwordvalidator.checkDigits("sheridan");
		Assert.assertTrue(result);
	}
	@Test
	public void testCheckDigitsHappyPath() {
		boolean result = Passwordvalidator.checkDigits("gps25645ldh");
		Assert.assertTrue(result);
	}
	
	
	
	
	
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	
}
